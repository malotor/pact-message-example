# Publish/Subscribe Events Middleware

## Consumer

```bash
$ make build
$ make bash
$ cd consumer
$ bundle install
$ bundle exec rspec
```

Contract will be generated in `spec/pacts`


## Producer

```bash
$ make build
$ make bash
$ cd consumer
$ bundle install
$ bundle exec rake pact:verify
```
