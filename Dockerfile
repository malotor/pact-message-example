FROM ruby:3.0

ENV APP /app
WORKDIR $APP

RUN apt-get update && \
    apt-get install -y build-essential
