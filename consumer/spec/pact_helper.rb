Pact.message_consumer "Zoo Consumer" do
  has_pact_with "Zoo Provider" do
    mock_provider :alice_producer do
      pact_specification_version '2'
    end
  end
end
