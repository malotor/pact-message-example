require 'pact'
require 'pact/message'

require "pact/message/consumer/rspec"
# require "fileutils"
require "pact/helpers"

require_relative "./pact_helper"

RSpec.describe "creating a message pact" do

  include Pact::Helpers

  class StringMessageHandler
    attr_reader :output_stream

    def initialize
      @output_stream = StringIO.new
    end

    def call(content_string)
      message = OpenStruct.new(JSON.parse(content_string))
      output_stream.print "Hello #{message.name}"
    end
  end

  context "with a string message" do
    let(:message_handler) { StringMessageHandler.new }

    it "allows a consumer to test that it can handle the expected message", pact: :message do
      alice_producer
        .given("there is an alligator named Mary")
        .is_expected_to_send("an alligator message")
        .with_metadata(type: 'animal')
        .with_content(name: "Mary")

      alice_producer.send_message_string do | content_string |
        p '*'*100
        p content_string.inspect
        message_handler.call(content_string)
      end

      expect(message_handler.output_stream.string).to eq ("Hello Mary")
    end
  end
end
