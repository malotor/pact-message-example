require 'pact'
require 'pact/message'

require_relative './provider_states'

class MyMessageProvider
  def create_hello_message
    {
      name: "Mary"
    }
  end
end

CONFIG = {
  "an alligator message" => lambda { MyMessageProvider.new.create_hello_message }
}

Pact.message_provider "MyMessageProvider" do
  honours_pact_with "Zoo Consumer" do
    pact_uri "../spec/pacts/zoo_consumer-zoo_provider.json"
  end

  builder do |description|
    CONFIG[description].call
  end
end
